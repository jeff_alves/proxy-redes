#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-
from socket import errno
import socket


def get_header(dados):
    end = dados.find('\r\n\r\n')
    if end != -1:
        return dados[:end]
    return None


def get_campo(campo, dados):
    header = get_header(dados)
    if header:
        ini = header.find(campo)
        if ini != -1:
            end = dados.find('\r\n', ini)
            return dados[ini:end].split(': ')[1]
    return None


def get_host(dados):
    host = get_campo('Host', dados)
    if host:
        host = host.split(':')
        port = 80
        if len(host) > 1:
            port = int(host[1])
        return host[0], port
    return None, None


def parser_header(dados):
    header = get_header(dados)
    if header:
        headers = header.splitlines()
        h = headers.pop(0).split()
        dic = {'Method': h[0], 'Url': h[1], 'Version': h[2]}
        for h in headers:
            h = h.split(': ', 1)
            if len(h) > 1:
                dic[h[0]] = h[1]
        if 'Host' in dic:
            dic['Port'] = 80
            h = dic['Host'].split(':', 1)
            if len(h) > 1:
                dic['Host'] = h[0]
                dic['Port'] = int(h[1])
        return dic
    return None


def conexao_peer(sock):
    try:
        return sock.getpeername()
    except Exception as e:  # @UnusedVariable
        return None


def get_ip():
    try:
        return ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])  # @IgnorePep8
    except socket.error as e:
        if e.errno == errno.ENETUNREACH:
            return '127.0.0.1'
        else:
            raise e
    except Exception as e:
        print('Erro1: ', type(e), e, e.message, e.__doc__)
        return '127.0.0.1'
