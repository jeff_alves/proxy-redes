Trabalho de Redes
----------------------
Servidor de proxy HTTP usando sockets bloqueante, não bloqueante e threads.
Aluno: Jefferson Alves
https://bitbucket.org/jeff_alves/proxy-redes


Requisitos
----------------------
Python 2.7		-> sudo apt-get install python2.7
Testado em Linux


Como usar (Opções entre colchetes são opcionais)
----------------------
Iniciar o servidor Proxy:
proxy_server.py [-h] [--host HOST] [--port PORT]

Ex: python proxy_server.py

*Configurar a proxy do S.O. para usar o servidor, porta padrão 1111