#!/usr/local/bin/python2.7
# -*- coding: utf-8 -*-

'''
Created on 17 de jun de 2016
@author: jeff
'''

from threading import Thread
import argparse
import select
import socket
from util import conexao_peer, get_ip, parser_header


class Cliente(Thread):

    def __init__(self, con_navegador, address, name):
        Thread.__init__(self)
        self.con_navegador = con_navegador
        self.address = address
        self.name = name
        self.con_navegador.setblocking(0)
        self.setDaemon(True)

        self.b_size = 8192
        self.ativo = True
        self.con_internet = None
        self.inputs = [self.con_navegador]

    def run(self):
        while self.ativo:
            try:
                r, w, e = select.select(self.inputs, [], [])  # @UnusedVariable
                for con in r:
                    dados = con.recv(self.b_size)
                    if con == self.con_navegador:
                        if dados:
                            self.navegador_handler(dados)
                        else:
                            print('Encerrando conexao do ' + self.name + ' (Pelo navegador): ' + str(conexao_peer(con)))
                            self.stop()
                    else:
                        if dados:
                            self.internet_handler(dados)
                        else:
                            print('Encerrando conexao com internet: ' +
                                  str(conexao_peer(con)))
                            self.finalizar_con(con)
                            print('Encerrando conexao do ' + self.name + ' (pela internet): ' + str(conexao_peer(self.con_navegador)))
                            self.stop()
            except Exception as e:
                print('Encerrando conexao do ' + self.name + ' ' + str(e))
                self.stop()

    def stop(self):
        self.ativo = False
        self.finalizar_con(self.con_navegador)
        for con in self.inputs:
            self.finalizar_con(con)
        exit(0)

    def finalizar_con(self, con):
        if con in self.inputs:
            self.inputs.remove(con)
        con.close()
        con = None

    def conectar_internet(self, host, port):
        try:
            print('Abrindo conexao com internet: %s:%d' % (host, port))
            self.con_internet = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.con_internet.connect((host, port))
            self.con_internet.setblocking(0)
            self.inputs.append(self.con_internet)
            print('Conexao com internet aberta: %s:%d' % (host, port))
        except Exception as e:
            raise Exception(str(e) + ': ' + str(host) + ':' + str(port))

    def navegador_handler(self, dados):
        print(self.name + ' -> Proxy: %s' % dados.split('\r\n', 1)[0])
        if not conexao_peer(self.con_internet):
            dic = parser_header(dados)
            if 'Host' in dic:
                if dic['Method'] == 'CONNECT':  # HTTPS
                    raise Exception('[Errno]: Ignorando HTTPS:' + str(dic))
                self.conectar_internet(dic['Host'], dic['Port'])
            else:
                raise Exception('[Errno]: Sem campo Host para abrir conexao')
        print('Proxy -> Internet: %s' % dados.split('\r\n', 1)[0])
        self.enviar(self.con_internet, dados)

    def internet_handler(self, dados):
        if dados.startswith('HTTP'):
            print('Internet -> Proxy -> ' + self.name + ': %s' % dados.split('\r\n', 1)[0])
        self.enviar(self.con_navegador, dados)

    def enviar(self, con, dados):
        if conexao_peer(con):
            con.sendall(dados)


class Proxy():

    def __init__(self, host, port):
        self.address = (host, port)
        self.backlog = 10
        self.b_size = 1024
        self.ativo = True

    def start_listening(self):
        try:
            self.server_con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server_con.setsockopt(
                socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.server_con.bind(self.address)
            self.server_con.listen(self.backlog)
            print('Servidor iniciado em: %s:%d' % (get_ip(), self.address[1]))
            i = 1
            while self.ativo:
                cli_con, cli_address = self.server_con.accept()
                cli = Cliente(cli_con, cli_address, 'Navegador(' + str(i) + ')')
                print('Nova conexao: ' + cli.name + ' - %s:%d' % cli_address)
                cli.start()
                i += 1
        except Exception as e:
            print('Exception2', e)
            exit(1)

    def stop_listening(self):
        self.ativo = False
        print('Finalizando Servidor')
        self.server_con.close()
        exit(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Servidor proxy de HTTP')
    parser.add_argument('--host', default='')
    parser.add_argument('--port', default='1111')
    args = parser.parse_args()

    try:
        server = Proxy(args.host, int(args.port))
        server.start_listening()
    except KeyboardInterrupt:
        print('Ctrl+C')
        server.stop_listening()
        exit(1)
